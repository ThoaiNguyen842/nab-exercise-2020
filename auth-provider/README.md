# Introduction


This project is intended to implement the "Authentication Provider" service of Icommerce project that is a Web API to generate JWT based on a Facebook access token:


- Tech stack: Spring boot, Containerize using Docker, JWT, JPA/Hibernate, Spring circuit breaker (using when communicating with Facebook Graph API)


## Project structure


This project is Spring boot project so its structure is follow Spring bot's that includes:

- /icommerce-auth-provider/src/main/java: main code base that contains all implementation such as Controller, Business Logic, Dao access layer,...

- /icommerce-auth-provider/src/test/java: unit test and integration test cases

- /icommerce-auth-provider/src/main/resources: configuration profiles

- /icommerce-auth-provider/Dockerfile: docker build file


## Requirements


For building and running the application you need:


- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

- [Maven 3](https://maven.apache.org)

- [Docker] (optional)


## Running the application locally


There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `/icommerce-auth-provider/src/main/java/com/nab/Application.java` class from your IDE.


Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:


```shell

mvn spring-boot:run

```

Access to Swagger document to see full CURL commands to verify the APIs: [http://localhost:8080/swagger-ui.html](http://localhost:8081/swagger-ui.html)


## Run test

Test application

Libraries:

 - JUnit, Mockito 


```

mvn clean test

```



## Build docker image



```shell

mvn install


docker build -t PATH  .


```



## Run docker image


```shell

docker run -p 8080:8080 -t PATH


```
