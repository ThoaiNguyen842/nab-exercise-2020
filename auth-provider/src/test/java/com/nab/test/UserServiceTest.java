package com.nab.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.io.IOException;
import java.math.BigInteger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import com.nab.domain.User;
import com.nab.dto.FacebookDebugReponseDto;
import com.nab.dto.LoginReponseDto;
import com.nab.dto.LoginReponseStatus;
import com.nab.dto.LoginRequestDto;
import com.nab.repository.RoleDao;
import com.nab.repository.UserDao;
import com.nab.service.FacebookGraphService;
import com.nab.service.JWTService;
import com.nab.service.UserServiceImpl;

public class UserServiceTest {
  private static final Integer CUSTOMER_ROLE = 1;
  private static final Integer COMPANY_ADMIN_ROLE = 2;
  private static final String CUSTOMTER_NEW_VAILD_ACCESS_TOKEN = "ashsdgdg";
  private static final String CUSTOMTER_NEW_INVAILD_ACCESS_TOKEN = "ashsdgdginvaild";
  private static final String CUSTOPM_EXISTING_VAILD_ACCESS_TOKEN = "nbhdhdd";
  private UserDao userDao;
  private RoleDao roleDao;
  private UserServiceImpl userService;
  private JWTService jwtService;
  private FacebookGraphService facebookGraphService;
  @Value("${jaw.key}")
  private String jwtBase64Key;
  
  @BeforeEach
  public void initialize() throws IOException, InterruptedException {
    userDao = Mockito.mock(UserDao.class);
    roleDao = Mockito.mock(RoleDao.class);
    jwtService =  Mockito.mock(JWTService.class);
    facebookGraphService =  Mockito.mock(FacebookGraphService.class);
    userService = new UserServiceImpl(userDao, roleDao, jwtService, CUSTOMER_ROLE, facebookGraphService);
    
    //mock facebook access token
    FacebookDebugReponseDto fReponseDto1 = new FacebookDebugReponseDto("123");
    Mockito.when(facebookGraphService.verifyAccessToken(CUSTOMTER_NEW_VAILD_ACCESS_TOKEN)).thenReturn(fReponseDto1);
    
    FacebookDebugReponseDto fReponseDto2 = new FacebookDebugReponseDto("345");
    Mockito.when(facebookGraphService.verifyAccessToken(CUSTOPM_EXISTING_VAILD_ACCESS_TOKEN)).thenReturn(fReponseDto2);
    
    //mock a new user created by a Facebook access token
    User newCustomer = new User();
    newCustomer.setId(BigInteger.valueOf(1L));
    newCustomer.setIsActive(User.ACTIVE_STATUS);
    newCustomer.setRole(roleDao.findRoleById(CUSTOMER_ROLE));
    Mockito.when(userDao.save(Mockito.any(User.class))).thenReturn(newCustomer);
    
    User existingUser = new User();
    existingUser.setId(BigInteger.valueOf(2L));
    existingUser.setIsActive(User.ACTIVE_STATUS);
    existingUser.setRole(roleDao.findRoleById(COMPANY_ADMIN_ROLE));
    Mockito.when(userDao.findUserbyFacebookId(CUSTOPM_EXISTING_VAILD_ACCESS_TOKEN)).thenReturn(existingUser);
    
    //mock JWT
    Mockito.when(jwtService.issue(Mockito.any(User.class))).thenReturn("hasjdgashdg");
  }
  
  @Test
  public void testVaildCustomerRegister() {
    LoginRequestDto loginRequestDto = new LoginRequestDto();
    loginRequestDto.setAccessToken(CUSTOMTER_NEW_VAILD_ACCESS_TOKEN);
    LoginReponseDto loginReponseDto1 = userService.login(loginRequestDto);
    assertEquals("Customer with vaild FB access token must register sucessfully", loginReponseDto1.getStatus(), LoginReponseStatus.SUCCESS);
    assertTrue("A JWT must be issued", !StringUtils.isEmpty(loginReponseDto1.getJwt()));
  }
  
  @Test
  public void testInVaildCustomerRegister() {
    LoginRequestDto loginRequestDto = new LoginRequestDto();
    loginRequestDto.setAccessToken(CUSTOMTER_NEW_INVAILD_ACCESS_TOKEN);
    LoginReponseDto loginReponseDto1 = userService.login(loginRequestDto);
    assertEquals("Customer with invaild FB access token must not register sucessfully", loginReponseDto1.getStatus(), LoginReponseStatus.FAIL);
  }
  
  @Test
  public void testExistingCustomerLogin() {
    LoginRequestDto loginRequestDto = new LoginRequestDto();
    loginRequestDto.setAccessToken(CUSTOPM_EXISTING_VAILD_ACCESS_TOKEN);
    LoginReponseDto loginReponseDto1 = userService.login(loginRequestDto);
    assertEquals("Customer with vaild FB access token must register sucessfully", loginReponseDto1.getStatus(), LoginReponseStatus.SUCCESS);
    assertTrue("A JWT must be issued", !StringUtils.isEmpty(loginReponseDto1.getJwt()));
  }
}
