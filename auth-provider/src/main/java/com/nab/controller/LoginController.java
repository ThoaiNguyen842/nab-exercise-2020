package com.nab.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.nab.dto.LoginReponseDto;
import com.nab.dto.LoginReponseStatus;
import com.nab.dto.LoginRequestDto;
import com.nab.service.UserService;

@NabController
public class LoginController extends BaseController {
  private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);
  private final UserService userService;

  @Autowired
  public LoginController(UserService userService) {
    this.userService = userService;
  }

  @PostMapping(path = "/issue", produces = {"application/json"})
  public ResponseEntity<LoginReponseDto> issue(@Valid @RequestBody LoginRequestDto loginRequestDto,
      HttpServletRequest request) {
    LOGGER.info("user login {}", loginRequestDto);
    LoginReponseDto loginReponseDto = userService.login(loginRequestDto);
    if (loginReponseDto != null && loginReponseDto.getStatus() == LoginReponseStatus.SUCCESS) {
      return ResponseEntity.ok(loginReponseDto);
    } else {
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
    }
  }
}
