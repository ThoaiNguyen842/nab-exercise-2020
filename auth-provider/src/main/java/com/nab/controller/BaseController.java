package com.nab.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import com.nab.controller.exceptions.AppExceptionHandler;

public abstract class BaseController {
  @Autowired
  private AppExceptionHandler exceptionHandler;

  @ExceptionHandler
  public ResponseEntity handleInternalServerError(Exception ex) {
    return exceptionHandler.handleException(ex);
  }
}
