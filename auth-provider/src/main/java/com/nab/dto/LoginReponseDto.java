package com.nab.dto;

public class LoginReponseDto {
  public LoginReponseDto(LoginReponseStatus status, String jwt) {
    super();
    this.jwt = jwt;
    this.status = status;
  }

  private LoginReponseStatus status;
  private String jwt;

  public String getJwt() {
    return jwt;
  }

  public void setJwt(String jwt) {
    this.jwt = jwt;
  }

  public LoginReponseStatus getStatus() {
    return status;
  }

  public void setStatus(LoginReponseStatus status) {
    this.status = status;
  }

}
