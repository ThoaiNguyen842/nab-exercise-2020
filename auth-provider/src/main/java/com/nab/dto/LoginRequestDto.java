package com.nab.dto;

import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Thoai_Nguyen
 */
public class LoginRequestDto {
  @NotNull
  @JsonProperty
  private String accessToken;
  public String getAccessToken() {
    return accessToken;
  }
  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }
}
