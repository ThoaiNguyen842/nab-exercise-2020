package com.nab.dto;

public class FacebookDebugReponseDto {
  public FacebookDebugReponseDto(String facebookId) {
    this.facebookId = facebookId;
  }

  public FacebookDebugReponseDto() {

  }

  private String facebookId;

  public String getFacebookId() {
    return facebookId;
  }

  public void setFacebookId(String facebookId) {
    this.facebookId = facebookId;
  }
}
