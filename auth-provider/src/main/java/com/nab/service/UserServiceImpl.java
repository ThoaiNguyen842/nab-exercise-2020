package com.nab.service;

import java.io.IOException;
import java.util.Calendar;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import com.nab.domain.Role;
import com.nab.domain.User;
import com.nab.dto.FacebookDebugReponseDto;
import com.nab.dto.LoginReponseDto;
import com.nab.dto.LoginReponseStatus;
import com.nab.dto.LoginRequestDto;
import com.nab.repository.RoleDao;
import com.nab.repository.UserDao;
import com.nab.util.PermissionCode;

@Service
@Transactional
public class UserServiceImpl implements UserService {
  private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
  private final UserDao userDao;
  private final RoleDao roleDao;
  private final Integer defaultRoleId;
  private final JWTService jwtService;
  private final FacebookGraphService facebookGraphService;

  @Autowired
  public UserServiceImpl(UserDao userDao, RoleDao roleDao, JWTService jwtService,
      @Value("${app.default_user_role}") Integer defaultRoleId,
      FacebookGraphService facebookGraphService) {
    this.userDao = userDao;
    this.defaultRoleId = defaultRoleId;
    this.jwtService = jwtService;
    this.facebookGraphService = facebookGraphService;
    this.roleDao = roleDao;
  }

  @Override
  public LoginReponseDto login(LoginRequestDto loginRequestDto) {
    try {
      // verify user access token by calling to Facebook debug API
      FacebookDebugReponseDto fGraphReponseDto =
          facebookGraphService.verifyAccessToken(loginRequestDto.getAccessToken());
      if (fGraphReponseDto != null && !StringUtils.isEmpty(fGraphReponseDto.getFacebookId())) {
        User user = userDao.findUserbyFacebookId(fGraphReponseDto.getFacebookId());
        // register a new user if this is the first time customer visits website
        if (user == null) {
          User newUser = new User();
          newUser.setFacebookId(fGraphReponseDto.getFacebookId());
          newUser.setIsActive(User.ACTIVE_STATUS);
          Role defaultRole = roleDao.findRoleById(defaultRoleId);
          newUser.setRole(defaultRole);
          newUser.setCreatedDate(Calendar.getInstance());
          userDao.save(newUser);
          user = newUser;
        }
        //user must be active and has login permission
        if (user.getIsActive() == user.ACTIVE_STATUS) {
          String jwt = jwtService.issue(user);
          return new LoginReponseDto(LoginReponseStatus.SUCCESS, jwt);
        }
      }
    } catch (IOException | InterruptedException e) {
      LOGGER.info("can not verify user access token {}", e);
    }
    return new LoginReponseDto(LoginReponseStatus.FAIL, null);
  }
}
