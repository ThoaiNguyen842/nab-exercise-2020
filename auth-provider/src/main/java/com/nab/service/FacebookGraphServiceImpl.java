package com.nab.service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import javax.security.sasl.AuthenticationException;
import org.apache.http.HttpStatus;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import com.nab.dto.FacebookDebugReponseDto;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
@Profile("prod")
public class FacebookGraphServiceImpl implements FacebookGraphService {
  private final String appAccessToken;
  private final String baseUrl;

  @Autowired
  public FacebookGraphServiceImpl(@Value("${facebook.app-access-token}") String appAccessToken,
      @Value("${facebook.base-url}") String baseUrl) {
    this.appAccessToken = appAccessToken;
    this.baseUrl = baseUrl;
  }

  @HystrixCommand(fallbackMethod = "reliable")
  @Override
  public FacebookDebugReponseDto verifyAccessToken(String accessToken)
      throws IOException, InterruptedException {
    String debugTokenURL = String.format(baseUrl + "/debug_token?input_token=s&access_token=%s'",
        accessToken, appAccessToken);
    HttpClient client = HttpClient.newHttpClient();
    HttpRequest request = HttpRequest.newBuilder().uri(URI.create(debugTokenURL)).build();
    HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
    if (response.statusCode() == HttpStatus.SC_OK) {
      JSONObject data = new JSONObject(response.body());
      if (data.has("user_id")) {
        return new FacebookDebugReponseDto(data.getString("user_id"));
      }
    }
    throw new AuthenticationException();
  }

  public FacebookDebugReponseDto reliable(String accessToken) {
    return null;
  }
}
