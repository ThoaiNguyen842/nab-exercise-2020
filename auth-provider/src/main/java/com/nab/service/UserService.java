package com.nab.service;

import java.io.IOException;
import com.nab.dto.LoginReponseDto;
import com.nab.dto.LoginRequestDto;

public interface UserService {
  LoginReponseDto login(LoginRequestDto loginRequestDto);
}
