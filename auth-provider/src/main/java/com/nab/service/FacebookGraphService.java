package com.nab.service;

import java.io.IOException;
import com.nab.dto.FacebookDebugReponseDto;

public interface FacebookGraphService {
  FacebookDebugReponseDto verifyAccessToken(String accessToken) throws IOException, InterruptedException; 
}
