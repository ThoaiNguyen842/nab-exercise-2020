package com.nab.service;

import com.nab.domain.User;

public interface JWTService {
  String issue(User user);
}
