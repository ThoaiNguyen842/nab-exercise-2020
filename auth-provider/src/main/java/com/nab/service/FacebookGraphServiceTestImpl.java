package com.nab.service;

import java.io.IOException;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import com.nab.dto.FacebookDebugReponseDto;

/**
 * @author Thoai_Nguyen
 * this service is used to bypass Facebook access token validation since I have no Facebook app account (sorry about this)
 * to see the implementation that works with real Facebook app account please look at {@link com.nab.service.FacebookGraphServiceImpl}
 */
@Service
@Profile("dev")
public class FacebookGraphServiceTestImpl implements FacebookGraphService {
  @Override
  public FacebookDebugReponseDto verifyAccessToken(String accessToken)
      throws IOException, InterruptedException {
    return new FacebookDebugReponseDto(accessToken);
  }
}
