package com.nab.service;

import java.util.Date;
import java.util.stream.Collectors;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import com.nab.domain.User;
import io.jsonwebtoken.CompressionCodecs;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * @author Thoai_Nguyen Should use asymmetry Algorithm to improve security perspective
 */
@Component
public class JWTServiceImpl implements JWTService {
  private final Long jwtExpireTime;

  private final byte[] key;

  @Autowired
  public JWTServiceImpl(@Value("${jaw.key}") String jwtKey,
      @Value("${jaw.expire-time}") Long jwtExpireTime) {
    key = Base64.decodeBase64(jwtKey);
    this.jwtExpireTime = jwtExpireTime;
  }

  public String issue(User user) {
    Date expireDate = new Date(System.currentTimeMillis() + jwtExpireTime);
    JwtBuilder b = Jwts.builder().setSubject(user.getId().toString()).setExpiration(expireDate);
    b.compressWith(CompressionCodecs.DEFLATE);
    b.claim("authorities", user.getRole().getRolePermissions().stream()
        .map(rp -> rp.getPermission().getCode()).collect(Collectors.toSet()));
    return b.signWith(SignatureAlgorithm.HS256, key).compact();
  }
}
