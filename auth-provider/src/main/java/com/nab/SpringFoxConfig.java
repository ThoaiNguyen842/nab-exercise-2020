package com.nab;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Import(springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration.class)
public class SpringFoxConfig {
  public static final String AUTHORIZATION_HEADER = "Authorization";

  @Bean
  public Docket api() {
    Contact contact = new Contact("NAB-excercise", "", "thoainguyen842@gmail.com");
    List<VendorExtension> vext = new ArrayList<>();
    ApiInfo apiInfo = new ApiInfo("Authentication provider", "NAB-excercise", "2.0.0", "", contact,
        "NAB-excercise", "", vext);
    Docket docket = new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo).pathMapping("/")
        .apiInfo(ApiInfo.DEFAULT).forCodeGeneration(true)
        .genericModelSubstitutes(ResponseEntity.class).ignoredParameterTypes(Pageable.class)
        .ignoredParameterTypes(java.sql.Date.class)
        .directModelSubstitute(java.time.LocalDate.class, java.sql.Date.class)
        .directModelSubstitute(java.time.ZonedDateTime.class, Date.class)
        .directModelSubstitute(java.time.LocalDateTime.class, Date.class)
        .useDefaultResponseMessages(false);
    docket = docket.select().paths(PathSelectors.ant("/auth/**")).build();
    return docket;
  }
}
