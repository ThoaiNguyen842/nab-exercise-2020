package com.nab.repository;

import java.util.List;
import javax.transaction.Transactional;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import com.nab.domain.User;

@Repository
@Transactional
public class UserDaoImpl extends BaseDaoImpl implements UserDao {
  @Override
  public User save(User userAccount) {
    getSession().save(userAccount);
    return userAccount;
  }

  @Override
  public User findUserbyFacebookId(String facebookId) {
    String hql = "FROM User u WHERE u.facebookId = :facebookId";
    Query query = getSession().createQuery(hql);
    query.setParameter("facebookId", facebookId);
    List results = query.list();
    if (!results.isEmpty()) {
      return (User) results.get(0);
    }
    return null;
  }
}
