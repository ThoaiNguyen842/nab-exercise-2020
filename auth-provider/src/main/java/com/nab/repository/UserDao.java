package com.nab.repository;

import com.nab.domain.User;

public interface UserDao {
  User save(User userAccount);
  User findUserbyFacebookId(String facebookId);
}
