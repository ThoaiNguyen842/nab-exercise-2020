package com.nab.repository;

import java.util.List;
import javax.transaction.Transactional;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import com.nab.domain.Role;

@Repository
@Transactional
public class RoleDaoImpl extends BaseDaoImpl implements RoleDao {

  @Override
  public Role findRoleById(Integer id) {
    String hql = "FROM Role r WHERE r.id = :id";
    Query query = getSession().createQuery(hql);
    query.setParameter("id", id);
    List results = query.list();
    if (!results.isEmpty()) {
      return (Role) results.get(0);
    }
    return null;
  }

}
