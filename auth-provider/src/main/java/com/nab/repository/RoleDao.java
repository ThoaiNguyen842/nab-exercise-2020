package com.nab.repository;

import com.nab.domain.Role;

public interface RoleDao {
  Role findRoleById(Integer id);
}
