
package com.nab.domain;

import java.util.Calendar;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "role")
public class Role {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  @Column(name = "name")
  private String name;
  @Column(name = "description")
  private String description;
  @Column(name = "created_date")
  private Calendar createdDate;
  @Column(name = "modified_date")
  private Calendar modifiedDate;
  @OneToMany(mappedBy = "role", cascade = {CascadeType.REMOVE}, fetch = FetchType.LAZY)
  private java.util.Set<User> userAccounts;
  @OneToMany(mappedBy = "role", cascade = {CascadeType.REMOVE}, fetch = FetchType.LAZY)
  private java.util.Set<RolePermission> rolePermissions;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Calendar getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Calendar createdDate) {
    this.createdDate = createdDate;
  }

  public Calendar getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(Calendar modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public java.util.Set<User> getUserAccounts() {
    return userAccounts;
  }

  public void setUserAccounts(java.util.Set<User> userAccounts) {
    this.userAccounts = userAccounts;
  }

  public java.util.Set<RolePermission> getRolePermissions() {
    return rolePermissions;
  }

  public void setRolePermissions(java.util.Set<RolePermission> rolePermissions) {
    this.rolePermissions = rolePermissions;
  }

}
