package com.nab.util;

public enum PermissionCode {
  PMS_001("PMS-001"), // login
  PMS_002("PMS-002"), // order products
  PMS_003("PMS-003"); // update product

  private final String code;

  private PermissionCode(final String code) {
    this.code = code;
  }

  @Override
  public String toString() {
    return code;
  }
}
