# Introduction

This project implements a back end services of "iCommerce" project that build a very simple online shopping application to sellproducts.

## Tech stack

JDK 8, Spring boot, Spring Security, Spring AOP, Spring circuit breaker, JPA/Hibernate, Docker, JWT, JUnit, Mockito

## Architecture design

The "ICommerce" backend system is applied micro-services architecture that includes several services inside. Currently, we are having two services that communicate via JWT mechanism (click to their name to see more detail):

- [Auth provider Service](https://bitbucket.org/ThoaiNguyen842/nab-exercise-2020/src/master/auth-provider/README.md): takes responsibility to issue JWT token based on user's Facebook access token that is generated at the Web App using Facebook Graph JavaScript SDK.
- [Ecommerce Service](https://bitbucket.org/ThoaiNguyen842/nab-exercise-2020/src/master/ecommerce-service/README.md): implements all business logic relating to Ecommerce such as load products, update product,... APIs at this service is authenticated via JWT issued at "Auth provider" Service

## How to get started?
You can easily start my services using my Docker image that available in Docker hub
```shell
docker run -p 8080:8080 -t thoainguyen842/icommerce-auth-provider
docker run -p 8081:8081 -t thoainguyen842/icommerce-ecommerce-service
```