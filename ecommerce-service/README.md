# Introduction

This project is intended to implement the "Ecommerce" service of Icommerce project that is a Web API that implement all business logic relates to eCommerce.

- Tech stack: Spring boot, Containerize using Docker, JWT, JSP/Hibernate, Spring Security, Spring AOP

## Project structure

This project is Spring boot project so its structure is follow Spring bot's that includes:
- /nab-ecommerce-service/src/main/java: main code base that contains all implementation such as Controller, Business Logic, Dao access layer,...
- /nab-ecommerce-service/src/test/java: unit test and integration test cases
- /nab-ecommerce-service/src/main/resources: configuration profiles
- /nab-ecommerce-service/Dockerfile: docker build file

## Requirements

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)
- [Docker] (optional)

## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `/nab-ecommerce-service/src/main/java/com/nab/Application.java` class from your IDE.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn spring-boot:run
```
Access to Swagger document to see full CURL commands to verify the APIs: [http://localhost:8081/swagger-ui.html](http://localhost:8081/swagger-ui.html), you will need to have a JWT that issued at "Auth provider" service to authenticate.

## Run unit test
Test application
Libraries:
 - JUnit, Mockito, SpringBootTest

```
mvn clean test
```


## Build docker image


```shell
mvn install

docker build -t PATH  .

```


## Run docker image

```shell
docker run -p 8081:8081 -t PATH

```