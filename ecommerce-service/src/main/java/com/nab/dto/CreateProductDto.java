package com.nab.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateProductDto {
  @JsonProperty
  @NotEmpty
  private String name;
  @JsonProperty
  @NotEmpty
  private String color;
  @JsonProperty
  @DecimalMin(value = "1", inclusive = true, message = "Price must be greater than 0")
  private BigDecimal price;
  @JsonProperty
  @DecimalMin(value = "1", inclusive = true, message = "Price must be greater than 0")
  private BigInteger branId;

  public String getName() {
    return name;
  }

  public String getColor() {
    return color;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public BigInteger getBranId() {
    return branId;
  }
}
