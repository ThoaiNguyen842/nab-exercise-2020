package com.nab.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SearchProductDto {
  @JsonProperty
  private String productName;
  @JsonProperty
  private String color;
  @JsonProperty
  private BigInteger brandId;
  @JsonProperty
  private BigDecimal fromPrice;
  @JsonProperty
  private BigDecimal toPrice;
  @JsonProperty
  private String sort;
  @JsonProperty
  private Integer start;
  @JsonProperty
  private Integer end;

  public String getProductName() {
    return productName;
  }

  public String getColor() {
    return color;
  }

  public BigInteger getBrandId() {
    return brandId;
  }

  public BigDecimal getFromPrice() {
    return fromPrice;
  }

  public BigDecimal getToPrice() {
    return toPrice;
  }

  public String getSort() {
    return sort;
  }

  public Integer getStart() {
    return start;
  }

  public Integer getEnd() {
    return end;
  }

}
