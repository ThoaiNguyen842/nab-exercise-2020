package com.nab.aspect;

import java.math.BigInteger;
import java.util.Arrays;
import javax.servlet.http.HttpSession;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.nab.domain.ActivityName;
import com.nab.service.ActivityService;
import com.nab.util.Costant;

@Aspect
@Component
public class ActivityAspect {
  private static final Logger LOGGER = LoggerFactory.getLogger(ActivityAspect.class);
  @Autowired
  private ActivityService activityService;;

  @Autowired
  private HttpSession session;

  @Pointcut("within(@com.nab.controller.NabController *)")
  public void springBeanPointcut() {}

  @Around("springBeanPointcut()")
  public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
    LOGGER.info("Enter: {}.{}() with argument[s] = {}",
        joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName(),
        Arrays.toString(joinPoint.getArgs()));
    BigInteger currentUserId =
        new BigInteger(session.getAttribute(Costant.SESSION_NAME_USER_ID).toString());
    switch (joinPoint.getSignature().getName()) {
      case "getProducts":
        activityService.save(currentUserId, BigInteger.ZERO, ActivityName.LOAD_PRODUCT);
        break;
      default:
        break;
    }
    Object result = joinPoint.proceed();
    return result;
  }
}
