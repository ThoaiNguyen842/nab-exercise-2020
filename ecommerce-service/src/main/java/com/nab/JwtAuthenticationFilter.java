package com.nab;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.tomcat.util.bcel.classfile.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.util.StringUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
  private static final Logger LOGGER = LoggerFactory.getLogger(JwtAuthenticationFilter.class);
  private String jwtSecret;

  public JwtAuthenticationFilter(AuthenticationManager authenticationManager, String jwtSecret) {
    this.jwtSecret = jwtSecret;
    this.setAuthenticationManager(authenticationManager);
  }

  private UsernamePasswordAuthenticationToken parseToken(HttpServletRequest request) {
    String token = request.getHeader(HttpHeaders.AUTHORIZATION);
    if (token != null && token.startsWith("Bearer ")) {
      String jwt = token.replace("Bearer ", "");
      try {
        Claims claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(jwt).getBody();
        String userId = claims.getSubject();
        if (StringUtils.isEmpty(userId)) {
          return null;
        }
        //authenticate success
        ArrayList<String> authorities = null;
        if (claims.get("authorities") != null) {
          authorities = claims.get("authorities", ArrayList.class);
          System.out.println(authorities);
        }
        //add user id to session
        HttpSession session = request.getSession();
        session.setAttribute(com.nab.util.Costant.SESSION_NAME_USER_ID, userId);
        return new UsernamePasswordAuthenticationToken(userId, null,
            getGrantedAuthorities(authorities));
      } catch (JwtException exception) {
        LOGGER.warn("Some exception : {} failed : {}", token, exception.getMessage());
      }
    }
    return null;
  }

  private final List<GrantedAuthority> getGrantedAuthorities(final List<String> privileges) {
    final List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
    for (final String privilege : privileges) {
      authorities.add(new SimpleGrantedAuthority(privilege));
    }
    return authorities;
  }

  @Override
  public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
      throws IOException, ServletException {
    UsernamePasswordAuthenticationToken authentication = parseToken((HttpServletRequest) req);

    if (authentication != null) {
      SecurityContextHolder.getContext().setAuthentication(authentication);
    } else {
      SecurityContextHolder.clearContext();
    }
    chain.doFilter(req, res);
  }
}
