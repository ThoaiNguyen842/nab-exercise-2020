package com.nab.domain;

import java.math.BigInteger;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "brand")
public class Brand {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private BigInteger id;
  @Column(name = "name")
  private String name;
  @OneToMany(mappedBy = "brand", cascade = {CascadeType.REMOVE}, fetch = FetchType.LAZY)
  java.util.Set<Product> products;

  public BigInteger getId() {
    return id;
  }

  public void setId(BigInteger id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public java.util.Set<Product> getProducts() {
    return products;
  }

  public void setProducts(java.util.Set<Product> products) {
    this.products = products;
  }
}
