package com.nab.domain;

public enum ActivityName {
  LOAD_PRODUCT, MAKE_ORDER, LOAD_PRODUCT_DETAIL
}
