package com.nab;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import com.nab.util.PermissionCode;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class JwtSecurityConfiguration extends WebSecurityConfigurerAdapter {
  @Value("${jaw.key}")
  private String jwtBase64Key;

  @Override
  public void configure(WebSecurity web) throws Exception {
    web.ignoring().antMatchers("/v2/api-docs", "/configuration/ui", "/swagger-resources/**",
        "/configuration/security", "/swagger-ui.html", "/webjars/**");
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.cors().and().csrf().disable()
        .addFilter(new JwtAuthenticationFilter(authenticationManager(), jwtBase64Key))
        .authorizeRequests(authorizeRequests ->
             authorizeRequests
             //customer can get products to order
            .antMatchers(HttpMethod.GET, "/ecommerce/products").hasAnyAuthority(PermissionCode.PMS_002.toString())
             //company admin can insert/modify/delete product
            .antMatchers(HttpMethod.POST, "/ecommerce/products").hasAnyAuthority(PermissionCode.PMS_003.toString())
            .antMatchers(HttpMethod.PUT, "/ecommerce/products").hasAnyAuthority(PermissionCode.PMS_003.toString())
            .antMatchers(HttpMethod.DELETE, "/ecommerce/products").hasAnyAuthority(PermissionCode.PMS_003.toString())
            .anyRequest().authenticated())
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
  }
}
