package com.nab.controller;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import com.nab.dto.CreateProductDto;
import com.nab.dto.ProductsDto;
import com.nab.service.ProductSerivce;

@NabController
public class ProductController extends BaseController {
  private final ProductSerivce productSerivce;

  @Autowired
  public ProductController(ProductSerivce productSerivce) {
    this.productSerivce = productSerivce;
  }

  @GetMapping(path = "/products", produces = {"application/json"})
  public ResponseEntity<ProductsDto> getProducts(@RequestParam Optional<String> productName,
      @RequestParam Optional<String> color, @RequestParam Optional<BigInteger> brandId,
      @RequestParam Optional<BigDecimal> fromPrice, @RequestParam Optional<BigDecimal> toPrice,
      @RequestParam Optional<String> sort, @RequestParam Optional<Integer> start,
      @RequestParam Optional<Integer> end, HttpServletRequest httpServletRequest) {

    return ResponseEntity.ok(productSerivce.findProducts(productName.orElse(null),
        color.orElse(null), brandId.orElse(null), fromPrice.orElse(null), toPrice.orElse(null),
        sort.orElse(null), start.orElse(null), end.orElse(null)));
  }

  @PostMapping(path = "/products", produces = {"application/json"})
  public ResponseEntity<BigInteger> saveProduct(@Valid @RequestBody CreateProductDto dto,
      HttpServletRequest httpServletRequest) {
    try {
      return ResponseEntity.ok(productSerivce.save(dto));
    } catch (IllegalArgumentException e) {
      return ResponseEntity.badRequest().body(null);
    }
  }

  @PutMapping(path = "/products/{id}", produces = {"application/json"})
  public ResponseEntity<BigInteger> updateProduct(@Valid @RequestBody CreateProductDto dto,
      BigInteger id, HttpServletRequest httpServletRequest) {
    try {
      return ResponseEntity.ok(productSerivce.update(id, dto));
    } catch (IllegalArgumentException e) {
      return ResponseEntity.badRequest().body(null);
    }
  }
}
