package com.nab.controller;

import java.math.BigInteger;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import com.nab.controller.exceptions.AppExceptionHandler;
import com.nab.util.Costant;

public abstract class BaseController {
  @Autowired
  private AppExceptionHandler exceptionHandler;
  
  @ExceptionHandler
  protected ResponseEntity handleInternalServerError(Exception ex) {
    return exceptionHandler.handleException(ex);
  }
  
  protected BigInteger getCurrentUserId(HttpServletRequest httpServletRequest) {
    String userId = httpServletRequest.getSession().getAttribute(Costant.SESSION_NAME_USER_ID).toString();
    return new BigInteger(userId);
  }
}
