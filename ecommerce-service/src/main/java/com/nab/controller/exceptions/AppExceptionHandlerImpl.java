package com.nab.controller.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * Global error handler for controller methods.
 */
@Component
public class AppExceptionHandlerImpl implements AppExceptionHandler {

  @Override
  public ResponseEntity<Error> handleException(Exception e) {
    if (e instanceof AppError) {
      AppError appError = (AppError) e;
      // TODO log error if status code is 5xx
      HttpStatus statusCode = HttpStatus.valueOf(appError.getStatusCode());
      return ResponseEntity.status(statusCode).body(new Error(appError));
    }
    if (e instanceof org.springframework.web.bind.MethodArgumentNotValidException) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST)
          .body(new Error("BAD_REQUEST", e.getMessage()));
    }
    // TODO log error
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
        .body(new Error("INTERNAL_SERVER_ERROR", "Internal server error, please try again."));
  }
}
