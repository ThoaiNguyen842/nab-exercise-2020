package com.nab.controller.exceptions;

import org.springframework.http.ResponseEntity;

public interface AppExceptionHandler {
  ResponseEntity<Error> handleException(Exception e);
}
