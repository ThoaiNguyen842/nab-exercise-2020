package com.nab.controller.exceptions;

class Error {

  private String errorCode;
  private String errorMessage;

  public Error(String errorCode, String errorMessage) {
    this.errorCode = errorCode;
    this.errorMessage = errorMessage;
  }

  public Error(AppError e) {
    this.errorCode = e.getErrorCode();
    this.errorMessage = e.getErrorMessage();
  }

  public String getErrorCode() {
    return errorCode;
  }

  public String getErrorMessage() {
    return errorMessage;
  }
}
