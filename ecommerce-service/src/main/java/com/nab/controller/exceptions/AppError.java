package com.nab.controller.exceptions;

/**
 * Contract to for custom exception
 */
public interface AppError {

  /**
   * Return error message
   * 
   * @return
   */
  String getErrorMessage();

  /**
   * Return the specified HTTP status code.
   * 
   * @return
   */
  int getStatusCode();

  /**
   * Return message code for localized error message.
   * 
   * @return
   */
  String getErrorCode();
}
