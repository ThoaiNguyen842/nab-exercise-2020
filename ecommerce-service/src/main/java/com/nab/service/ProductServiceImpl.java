package com.nab.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.nab.domain.Brand;
import com.nab.domain.Product;
import com.nab.dto.CreateProductDto;
import com.nab.dto.ProductDto;
import com.nab.dto.ProductsDto;
import com.nab.repository.BrandDao;
import com.nab.repository.ProductDao;

@Service
@Transactional
public class ProductServiceImpl implements ProductSerivce {
  private final ProductDao productDao;
  private final BrandDao brandDao;

  @Autowired
  public ProductServiceImpl(ProductDao productDao, BrandDao brandDao) {
    this.productDao = productDao;
    this.brandDao = brandDao;
  }

  @Override
  public ProductsDto findProducts(String productName, String color, BigInteger brandId,
      BigDecimal fromPrice, BigDecimal toPrice, String sort, Integer start, Integer end) {
    ProductsDto productsDto = new ProductsDto();
    List<Product> products =
        productDao.findProducts(productName, color, brandId, fromPrice, toPrice, sort, start, end);
    List<ProductDto> listProductDto = products.stream().map(p -> {
      ProductDto productDto = new ProductDto();
      BeanUtils.copyProperties(p, productDto);
      return productDto;
    }).collect(Collectors.toList());
    productsDto.setProducts(listProductDto);
    return productsDto;
  }

  @Override
  public BigInteger save(CreateProductDto dto) {
    Brand brand = brandDao.findBrandById(dto.getBranId());
    if (brand == null) {
      throw new IllegalArgumentException("Brand not found");
    }
    Product product = new Product();
    BeanUtils.copyProperties(dto, product);
    product.setBrand(brand);
    return productDao.save(product).getId();
  }

  @Override
  public BigInteger update(BigInteger id, CreateProductDto dto) throws IllegalArgumentException {
    Brand brand = brandDao.findBrandById(dto.getBranId());
    if (brand == null) {
      throw new IllegalArgumentException("Brand not found");
    }
    Product product = productDao.findProductById(id);
    if (product == null) {
      throw new IllegalArgumentException("Product not found");
    }
    BeanUtils.copyProperties(dto, product);
    product.setBrand(brand);
    return productDao.save(product).getId();
  }
}
