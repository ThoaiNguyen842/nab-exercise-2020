package com.nab.service;

import java.math.BigInteger;
import java.util.Calendar;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.nab.domain.Activity;
import com.nab.domain.ActivityName;
import com.nab.repository.ActivityDao;

@Service
@Transactional
public class ActivityServiceImpl implements ActivityService {
  private final ActivityDao activityDao;

  @Autowired
  public ActivityServiceImpl(ActivityDao activityDao) {
    this.activityDao = activityDao;
  }

  @Override
  public void save(BigInteger userId, BigInteger refId, ActivityName activityName) {
    Activity activity = new Activity();
    activity.setActivity(activityName.toString());
    activity.setUserId(userId);
    activity.setReferenceId(refId);
    activity.setCreatedDate(Calendar.getInstance());
    activityDao.save(activity);
  }
}
