package com.nab.service;

import java.math.BigInteger;
import com.nab.domain.ActivityName;

public interface ActivityService {
  void save(BigInteger userId, BigInteger refId, ActivityName activityName);
}
