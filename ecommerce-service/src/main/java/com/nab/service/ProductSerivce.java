package com.nab.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import com.nab.dto.CreateProductDto;
import com.nab.dto.ProductsDto;

public interface ProductSerivce {
  ProductsDto findProducts(String productName, String color, BigInteger brandId,
      BigDecimal fromPrice, BigDecimal toPrice, String sort, Integer start, Integer end);

  BigInteger save(CreateProductDto dto);

  BigInteger update(BigInteger id, CreateProductDto dto) throws IllegalArgumentException;
}
