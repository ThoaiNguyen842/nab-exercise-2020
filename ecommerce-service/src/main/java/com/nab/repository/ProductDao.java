package com.nab.repository;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import com.nab.domain.Product;

public interface ProductDao {
  List<Product> findProducts(String productName, String color, BigInteger brandId,
      BigDecimal fromPrice, BigDecimal toPrice, String sort, Integer start, Integer end);

  Product save(Product product);
  
  Product findProductById(BigInteger id);
}
