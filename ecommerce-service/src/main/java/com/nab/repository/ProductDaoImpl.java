package com.nab.repository;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.transaction.Transactional;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import com.nab.domain.Product;

@Repository
@Transactional
public class ProductDaoImpl extends BaseDaoImpl implements ProductDao {

  @Override
  public List<Product> findProducts(String productName, String color, BigInteger brandId,
      BigDecimal fromPrice, BigDecimal toPrice, String sort, Integer start, Integer end) {
    StringBuilder queryString = new StringBuilder("from Product where id > 0 ");
    if (!StringUtils.isEmpty(productName)) {
      queryString.append(" AND lower(name) like lower(CONCAT('%', :productName, '%')) ");
    }
    if (!StringUtils.isEmpty(color)) {
      queryString.append(" AND lower(color) like lower(CONCAT('%', :color, '%')) ");
    }
    if (brandId != null && brandId.compareTo(BigInteger.ZERO) == 1) {
      queryString.append(" AND brand.id = :brandId ");
    }
    if (fromPrice != null && fromPrice.compareTo(BigDecimal.ZERO) == 1) {
      queryString.append(" AND price >= :fromPrice ");
    }
    if (toPrice != null && toPrice.compareTo(BigDecimal.ZERO) == 1) {
      queryString.append(" AND price <= :toPrice ");
    }
    if (!StringUtils.isEmpty(sort)) {
      queryString.append(" order by id " + " " + sort);
    }
    Query q = getSession().createQuery(queryString.toString());
    if (!StringUtils.isEmpty(productName)) {
      q.setParameter("productName", productName);
    }
    if (!StringUtils.isEmpty(color)) {
      q.setParameter("color", color);
    }
    if (brandId != null && brandId.compareTo(BigInteger.ZERO) == 1) {
      q.setParameter("brandId", brandId);
    }
    if (fromPrice != null && fromPrice.compareTo(BigDecimal.ZERO) == 1) {
      q.setParameter("fromPrice", fromPrice);
    }
    if (toPrice != null && toPrice.compareTo(BigDecimal.ZERO) == 1) {
      q.setParameter("toPrice", toPrice);
    }
    if (start != null && start >= 0) {
      q.setFirstResult(start);
    }
    if (end != null && end >= 0) {
      q.setMaxResults(end);
    }
    return q.list();
  }

  @Override
  public Product save(Product product) {
    getSession().save(product);
    return product;
  }

  @Override
  public Product findProductById(BigInteger id) {
    String hql = "FROM Product p WHERE p.id = :id";
    Query query = getSession().createQuery(hql);
    query.setParameter("id", id);
    List results = query.list();
    if (!results.isEmpty()) {
      return (Product) results.get(0);
    }
    return null;
  }

}
