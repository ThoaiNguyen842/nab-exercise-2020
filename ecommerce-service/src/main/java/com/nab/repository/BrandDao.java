package com.nab.repository;

import java.math.BigInteger;
import com.nab.domain.Brand;

public interface BrandDao {
  Brand findBrandById(BigInteger id);
}
