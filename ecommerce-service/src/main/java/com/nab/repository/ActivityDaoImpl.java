package com.nab.repository;

import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import com.nab.domain.Activity;

@Repository
@Transactional
public class ActivityDaoImpl extends BaseDaoImpl implements ActivityDao {

  @Override
  public Activity save(Activity activity) {
    getSession().save(activity);
    return activity;
  }

}
