package com.nab.repository;

import java.math.BigInteger;
import java.util.List;
import javax.transaction.Transactional;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import com.nab.domain.Brand;

@Repository
@Transactional
public class BrandDaoImpl extends BaseDaoImpl implements BrandDao {

  @Override
  public Brand findBrandById(BigInteger id) {
    String hql = "FROM Brand p WHERE p.id = :id";
    Query query = getSession().createQuery(hql);
    query.setParameter("id", id);
    List results = query.list();
    if (!results.isEmpty()) {
      return (Brand) results.get(0);
    }
    return null;
  }

}
