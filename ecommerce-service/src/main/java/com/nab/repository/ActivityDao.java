package com.nab.repository;

import com.nab.domain.Activity;

public interface ActivityDao {
  Activity save(Activity activity);
}
