package com.nab.test;

import static org.junit.Assert.assertTrue;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.nab.Application;
import com.nab.dto.ProductsDto;

@SpringBootTest(classes = Application.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class ProductAPIIntegrationTest {
  public static final String JWT_INVALID = "";
  public static final String JWT_CUSTOMER_4EVER_EXPIRE =
      "Bearer eyJhbGciOiJIUzI1NiIsInppcCI6IkRFRiJ9.eNqqViouTVKyUjI2NjYyUtJRSq0oULKyMDMxMDS1NDS0NLM0MNFRSiwtycgvyizJTC1WsopWCvAN1jUwMASqhrCMlGJrAQAAAP__.jmho3I19RHd1Nghuc9bdq_K0YjA4PaXS41TKGLoVc_E";
  public static final String JWT_ADMIN_4EVER_EXPIRE =
      "Bearer eyJhbGciOiJIUzI1NiIsInppcCI6IkRFRiJ9.eNqqViouTVKyUjI2NjYyUdJRSq0oULKyMDMxMDS1NDS0NLMwtdRRSiwtycgvyizJTC1WsopWCvAN1jUwMAaqhrAMlWJrAQAAAP__.Swkd-0eZ3a2Omg7s-F58JKqedLQBgSgVl7iOTQUs7u8";
  @LocalServerPort
  private int port;

  @Autowired
  private TestRestTemplate restTemplate;

  @Test
  public void testProductAPIs() {
    HttpHeaders headers = new HttpHeaders();
    headers.add("Authorization", JWT_CUSTOMER_4EVER_EXPIRE);
    HttpHeaders headers2 = new HttpHeaders();
    headers2.add("Authorization", JWT_INVALID);
    HttpHeaders headers3 = new HttpHeaders();
    headers2.add("Authorization", JWT_ADMIN_4EVER_EXPIRE);
    String baseURL = "http://localhost:" + port + "/ecommerce/products";

    // if JWT is not valid, user can not load product
    ResponseEntity<ProductsDto> response1 = restTemplate.exchange(baseURL, HttpMethod.GET,
        new HttpEntity<>(headers2), ProductsDto.class);
    System.out.println(response1.getStatusCode());
    assertTrue("unauthorized user can not see products",
        response1.getStatusCode().equals(HttpStatus.FORBIDDEN));

    // customer with valid JWT can load product to make order
    ResponseEntity<ProductsDto> response2 = restTemplate.exchange(baseURL, HttpMethod.GET,
        new HttpEntity<>(headers), ProductsDto.class);
    assertTrue("customer must be able to load products",
        response2.getStatusCode().equals(HttpStatus.OK));

    // customer can not add product
    ResponseEntity<Object> response3 =
        restTemplate.exchange(baseURL, HttpMethod.POST, new HttpEntity<>(headers), Object.class);
    assertTrue("cusomter is not allowed to add product",
        response3.getStatusCode().equals(HttpStatus.FORBIDDEN));

    // admin can not load products to make order
    ResponseEntity<ProductsDto> response4 = restTemplate.exchange(baseURL, HttpMethod.GET,
        new HttpEntity<>(headers3), ProductsDto.class);
    assertTrue("customer must be able to load products",
        response4.getStatusCode().equals(HttpStatus.FORBIDDEN));
  }
}
